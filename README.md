# GitLab triage operations

This repository is for adding triage labels to projects used by the Infrastructure, Platform teams.
It uses the [gitlab-triage](https://gitlab.com/gitlab-org/ruby/gems/gitlab-triage) Ruby gem that is the same Gem used by [triage-ops](https://gitlab.com/gitlab-org/quality/triage-ops) which is the main repository for triage operations for projects under `gitlab-org/`.

For setting up policies, the Infrastructure, Platform teams are not using sharing the same `triage-ops` repository as development due to differences in our policies, labels and helpers.

## Adding a new policy to an existing project

This project supports all teams in Infrastructure, Platform.
We aim to have consistent rules across teams and avoid duplication if possible.
For a reference on how to create the policy file see the [gitlab-triage documentation](https://gitlab.com/gitlab-org/ruby/gems/gitlab-triage#conditions-field) or look at other policy files in this repository.

For adding new rules is create it in the `policies/platform` directory:

1. Create a `.yml` file in `policies/platform/` (use other files as a reference if needed).
1. Add the file name to the `platform_policies:` for the teams that should have the policy applied.
1. Run `make generate-ci-config`
1. Create an MR and validate your rule in the dry-run output.

If you are sure the rule you are creating is now and forever will only be applicable to a single team, then it should be created in the `policies/<team>` directory:

1. Create a `.yml` file in `policies/<team>/` (use other files as a reference if needed).
1. Create an MR and validate your rule in the dry-run output.

## Adding a new project

If you wish to add a brand new project to triage-ops, do the following:

1. Add a new directory under `policies/`
1. Add a new element to the `projects` array in `.gitlab/ci/main.jsonnet`
1. Create a schedule with a corresponding variable that corresponds to the project
1. Run `make generate-ci-config`
1. Create an MR and validate the new CI jobs and the dry-run output.

## The schedules

At defined intervals [a pipeline schedule](https://gitlab.com/gitlab-com/gl-infra/triage-ops/pipeline_schedules)
will run for different teams.

Each of the schedules has a variable `sched_var` set in `.gitlab/ci/main.jsonnet`
These variables are used by schedules to ensure that only policies for specific schedule runs from the `master` branch.

For example, the `run:delivery` job in `.gitlab-ci.yml` has:

```yaml
variables:
  - $DELIVERY_PROJECT == '1'
```

The purpose of this variable is so we can separate schedules for every project.

Every MR will run all `dry-run` and create all `manual` jobs.

## The bot

We're using [@gitlab-bot](https://gitlab.com/gitlab-bot) as the user to run
triage operations. The credentials could be found in the shared 1Password
vault.
