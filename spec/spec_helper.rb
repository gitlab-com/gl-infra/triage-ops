# frozen_string_literal: true

require 'simplecov-cobertura'

SimpleCov.start do
  add_filter '/spec/'
  add_filter '/vendor/'
  # Use the cobertura formatter when tests are run in CI for code coverage
  formatter ENV['CI'] ? SimpleCov::Formatter::CoberturaFormatter : SimpleCov::Formatter::HTMLFormatter
end

require 'date'
require './lib/platform/slo_breach_helper'
require './lib/production/triage'
require './lib/gitlab-dedicated/triage'
