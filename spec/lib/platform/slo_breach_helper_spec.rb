# frozen_string_literal: true

require 'spec_helper'

describe Platform::SloBreachHelper do
  let(:today) { Date.today }
  let(:helper) { Class.new { extend Platform::SloBreachHelper } }

  before do
    allow(helper).to receive(:resource).and_return(
      created_at: Date.parse('2023-03-01'),
      labels: ['some-other-label', 'severity::2', 'team::Reliability']
    )
    allow(helper).to receive(:assignees).and_return(
      ['some-gitlab-username']
    )
  end

  describe '.slo_update_due_date_incident_review?' do
    before do
      allow(helper).to receive(:labels_with_details).and_return([])
    end
    it 'returns true when the due date is empty' do
      allow(helper).to receive(:resource).and_return(
        created_at: Date.parse('2023-03-01'),
        due_date: nil
      )
      expect(helper.slo_update_due_date_incident_review?).to eq(true)
    end

    it 'returns true when the due date is wrong' do
      allow(helper).to receive(:resource).and_return(
        created_at: Date.parse('2023-03-01'),
        due_date: '2023-10-01'
      )
      expect(helper.slo_update_due_date_incident_review?).to eq(true)
    end

    it 'returns false when the due date does not need to be updated' do
      allow(helper).to receive(:resource).and_return(
        created_at: Date.parse('2023-03-01'),
        due_date: '2023-03-15'
      )

      expect(helper.slo_update_due_date_incident_review?).to eq(false)
    end
  end

  describe 'slo_breach_date_incident_review' do
    it 'returns false if the due date does matches the slo for incident reviews' do
      expect(helper.slo_breach_date_incident_review).to eq(Date.parse('2023-03-15')) # 2023-03-01 + 14 days
    end
  end

  describe 'slo_breach_looming_incident_review?' do
    it 'returns true when the breach is looming' do
      allow(Date).to receive(:today).and_return(Date.parse('2023-03-09')) # 09 days after issue was created
      expect(helper.slo_breach_looming_incident_review?).to eq(true)
    end

    it 'returns false when the breach is not looming' do
      allow(Date).to receive(:today).and_return(Date.parse('2023-03-02')) # 1 day after issue was created
      expect(helper.slo_breach_looming_incident_review?).to eq(false)
    end

    it 'returns false when the slo is breached' do
      allow(Date).to receive(:today).and_return(Date.parse('2023-03-21')) # 20 days after issue was created
      expect(helper.slo_breach_looming_incident_review?).to eq(false)
    end
  end

  describe 'slo_breached_incident_review?' do
    it 'returns true when slo is breached' do
      allow(Date).to receive(:today).and_return(Date.parse('2023-03-16')) # 15 days after issue was created
      expect(helper.slo_breached_incident_review?).to eq(true)
    end

    it 'returns false when slo is not breached' do
      allow(Date).to receive(:today).and_return(Date.parse('2023-03-14')) # 13 days after the issue was created
      expect(helper.slo_breached_incident_review?).to eq(false)
    end
  end

  describe '.slo_breach_date' do
    it 'returns the breach date based on severity label details' do
      allow(helper).to receive(:labels_with_details).and_return(
        [
          double(
            name: 'severity::2',
            added_at: double(
              to_date: Date.parse('2023-03-04')
            )
          )
        ]
      )

      expect(helper.slo_breach_date).to eq(Date.parse('2023-04-03')) # 2023-03-04 + 30 days
    end
    it 'returns the breach date based on issue creation without label details' do
      allow(helper).to receive(:labels_with_details).and_return([])

      expect(helper.slo_breach_date).to eq(Date.parse('2023-03-31')) # 2023-03-01 + 30 days
    end
  end

  describe '.slo_breach_looming?' do
    before do
      allow(helper).to receive(:labels_with_details).and_return([])
    end
    it 'returns true when the breach is looming' do
      allow(Date).to receive(:today).and_return(Date.parse('2023-03-21')) # 20 days after issue was created
      expect(helper.slo_breach_looming?).to eq(true)
    end

    it 'returns false when the breach is not looming' do
      allow(Date).to receive(:today).and_return(Date.parse('2023-03-02')) # 1 day after issue was created
      expect(helper.slo_breach_looming?).to eq(false)
    end

    it 'returns false when the slo is breached' do
      allow(Date).to receive(:today).and_return(Date.parse('2023-04-01')) # 31 days after issue was created
      expect(helper.slo_breach_looming?).to eq(false)
    end
  end

  describe '.slo_breached?' do
    before do
      allow(helper).to receive(:labels_with_details).and_return([])
    end
    it 'returns true when slo is breached' do
      allow(Date).to receive(:today).and_return(Date.parse('2023-04-01')) # 31 days after issue was created
      expect(helper.slo_breached?).to eq(true)
    end

    it 'returns false when slo is not breached' do
      allow(Date).to receive(:today).and_return(Date.parse('2023-03-21')) # 20 days after the issue was created
      expect(helper.slo_breached?).to eq(false)
    end
  end

  describe '.team_label' do
    it 'selects the team::Reliability label' do
      expect(helper.team_label).to eq('team::Reliability')
    end
  end

  describe '.slo_days_til_breach' do
    before do
      allow(helper).to receive(:labels_with_details).and_return([])
    end
    it 'returns the correct number of days until breach' do
      allow(Date).to receive(:today).and_return(Date.parse('2023-03-21')) # 20 days after the issue was created
      expect(helper.slo_days_til_breach).to eq(10) # 10 days until 2023-03-31, for a severity::2 SLO of 30 days
    end
  end

  describe '.slo_update_due_date?' do
    before do
      allow(helper).to receive(:labels_with_details).and_return([])
    end
    it 'returns true when the due date is empty' do
      allow(helper).to receive(:resource).and_return(
        created_at: Date.parse('2023-03-01'),
        labels: ['severity::2', 'team::Reliability'],
        due_date: nil
      )
      expect(helper.slo_update_due_date?).to eq(true)
    end

    it 'returns true when the due date is wrong' do
      allow(helper).to receive(:resource).and_return(
        created_at: Date.parse('2023-03-01'),
        labels: ['severity::2', 'team::Reliability'],
        due_date: '2023-10-01'
      )
      expect(helper.slo_update_due_date?).to eq(true)
    end

    it 'returns false when the due date does not need to be updated' do
      allow(helper).to receive(:resource).and_return(
        created_at: Date.parse('2023-03-01'),
        labels: ['severity::2', 'team::Reliability'],
        due_date: '2023-03-31'
      )

      expect(helper.slo_update_due_date?).to eq(false)
    end
  end

  describe '.slo_breach_date_due' do
    before do
      allow(helper).to receive(:labels_with_details).and_return([])
    end
    it 'returns formatted breach date' do
      expect(helper.slo_breach_date_due).to eq('2023-03-31')
    end
  end
end
