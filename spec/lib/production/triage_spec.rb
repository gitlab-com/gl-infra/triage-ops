# frozen_string_literal: true

require 'spec_helper'

describe Production::Triage do
  let(:helper) { Class.new { extend Production::Triage } }

  describe '.exec_summary?' do
    it 'returns false when exec summary is the default' do
      allow(helper).to receive(:resource).and_return(
        description: <<~DESC
          some description blah blah blah
          ### :pencil: Summary for CMOC notice / Exec summary:

          1. Customer Impact: {+ Human-friendly 1-sentence statement on impacted +}
          1. Service Impact: {+ service:: labels of services impacted by this incident +}
          1. Impact Duration: {+ start time UTC +} - {+ end time UTC +} ({+ duration in minutes +})
          1. Root cause: {+ TBD +}
        DESC
      )

      expect(helper.exec_summary?).to eq(false)
    end

    it 'returns false when exec summary is missing filled in fields' do
      allow(helper).to receive(:resource).and_return(
        description: <<~DESC
          some description blah blah blah
          ### :pencil: Summary for CMOC notice / Exec summary:

          1. Customer Impact: {+ Human-friendly 1-sentence statement on impacted +}
          1. Service Impact: some service impact statement
          1. Impact Duration: some duration
          1. Root cause: some root cause
        DESC
      )

      expect(helper.exec_summary?).to eq(false)
    end
    it 'returns false when exec summary is missing' do
      allow(helper).to receive(:resource).and_return(
        description: <<~DESC
          some description blah blah blah
        DESC
      )

      expect(helper.exec_summary?).to eq(false)
    end

    it 'returns true when exec summary is fully filled out' do
      allow(helper).to receive(:resource).and_return(
        description: <<~DESC
          some description blah blah blah
          ### :pencil: Summary for CMOC notice / Exec summary:

          1. Customer Impact: some impact statement
          1. Service Impact: some service impact statement
          1. Impact Duration: some duration
          1. Root cause: some root cause
        DESC
      )

      expect(helper.exec_summary?).to eq(true)
    end

    it 'returns true when exec summary is fully filled out with different case/spacing' do
      allow(helper).to receive(:resource).and_return(
        description: <<~DESC
          some description blah blah blah
          ### :pencil: Summary for CMOC notice / Exec summary:

          1. customer Impact: some impact statement
          1. Service impact:  some service impact statement
          1. Impact duration : some duration
          1. Root Cause:   some root cause
        DESC
      )

      expect(helper.exec_summary?).to eq(true)
    end
  end

  describe '.high_severity?' do
    it 'returns true for sev1 incidents' do
      allow(helper).to receive(:resource).and_return(
        labels: %w[some-other-label Service::SomeService severity::1]
      )

      expect(helper.high_severity?).to eq(true)
    end
    it 'returns true for sev2 incidents' do
      allow(helper).to receive(:resource).and_return(
        labels: %w[some-other-label Service::SomeService severity::2]
      )

      expect(helper.high_severity?).to eq(true)
    end

    it 'returns false for sev3 incidents' do
      allow(helper).to receive(:resource).and_return(
        labels: %w[some-other-label Service::SomeService severity::3]
      )

      expect(helper.high_severity?).to eq(false)
    end
  end

  describe '.root_cause_needed?' do
    it 'returns true when root cause is needed' do
      allow(helper).to receive(:resource).and_return(
        labels: %w[some-other-label Service::SomeService]
      )
      expect(helper.root_cause_needed?).to eq(true)
    end

    it 'returns false when root cause is not needed' do
      allow(helper).to receive(:resource).and_return(
        labels: %w[some-other-label RootCause::NotNeeded]
      )
      expect(helper.root_cause_needed?).to eq(false)
    end
  end

  describe '.service_needed?' do
    it 'returns true when service is needed' do
      allow(helper).to receive(:resource).and_return(
        labels: %w[some-other-label RootCause::SomeRootCause]
      )
      expect(helper.service_needed?).to eq(true)
    end

    it 'returns false when service is not needed' do
      allow(helper).to receive(:resource).and_return(
        labels: %w[some-other-label Service::NotNeeded]
      )
      expect(helper.service_needed?).to eq(false)
    end
  end

  describe '.needed_labels' do
    it 'it returns all needed labels' do
      allow(helper).to receive(:resource).and_return(
        labels: %w[some-other-label Service::Needed RootCause::Needed CorrectiveActions::NotNeeded]
      )
      expect(helper.needed_labels).to eq(%w[Service::Needed RootCause::Needed])
    end
  end

  describe '.needed_label?' do
    it 'it returns true when there is a Service::Needed label' do
      allow(helper).to receive(:resource).and_return(
        labels: ['some-other-label', 'Service::Needed']
      )
      expect(helper.needed_label?).to eq(true)
    end

    it 'it returns true when there is a RootCause::Needed label' do
      allow(helper).to receive(:resource).and_return(
        labels: ['some-other-label', 'RootCause::Needed']
      )
      expect(helper.needed_label?).to eq(true)
    end

    it 'it returns true when there is a CorrectiveActions::Needed label' do
      allow(helper).to receive(:resource).and_return(
        labels: ['some-other-label', 'CorrectiveActions::Needed']
      )
      expect(helper.needed_label?).to eq(true)
    end

    it 'it returns false when there is not a needed label' do
      allow(helper).to receive(:resource).and_return(
        labels: ['some-other-label']
      )
      expect(helper.needed_label?).to eq(false)
    end
  end

  describe '.needed_mentions' do
    it 'it returns usernames for mention with assignees for incidents' do
      allow(helper).to receive(:resource).and_return(
        assignees: [{ username: 'some-user' }, { username: 'another-user' }],
        labels: []
      )
      expect(helper.needed_mentions).to eq(%w[some-user another-user kkyrala jarv])
    end
    it 'it returns usernames for mention without assignees' do
      allow(helper).to receive(:resource).and_return(
        assignees: [],
        labels: []
      )
      expect(helper.needed_mentions).to eq(%w[kkyrala jarv])
    end
    it 'it adds delivery leadership for release-blocker issues ' do
      allow(helper).to receive(:resource).and_return(
        assignees: [],
        labels: ['release-blocker']
      )
      expect(helper.needed_mentions).to eq(%w[kkyrala jarv dawsmith mbursi])
    end
  end

  describe '.corrective_actions' do
    it 'returns true with corrective actions' do
      allow(helper).to receive(:query_issue_links).and_return(
        [
          { 'labels' => ['some-label', 'another-label', 'corrective action'] },
          { 'labels' => %w[some-label another-label] }
        ]
      )
      expect(helper.corrective_actions?).to eq(true)
    end
    it 'returns false with no corrective actions' do
      allow(helper).to receive(:query_issue_links).and_return(
        [
          { 'labels' => %w[some-label another-label] },
          { 'labels' => %w[some-label another-label] }
        ]
      )
      expect(helper.corrective_actions?).to eq(false)
    end
  end

  describe '.incident_review_needed?' do
    context 'when there are no related issues with the incident-review label' do
      before do
        allow(helper).to receive(:query_issue_links).and_return(
          [
            { 'labels' => %w[some-label another-label] },
            { 'labels' => %w[some-label another-label] }
          ]
        )
      end

      it 'returns false for backstage issues' do
        allow(helper).to receive(:resource).and_return(
          labels: ['backstage']
        )
        expect(helper.incident_review_needed?).to eq(false)
      end

      it 'returns false for severity::3 issues' do
        allow(helper).to receive(:resource).and_return(
          labels: ['severity::3']
        )
        expect(helper.incident_review_needed?).to eq(false)
      end

      it 'returns true for review-requested issues' do
        allow(helper).to receive(:resource).and_return(
          labels: ['review-requested']
        )
        expect(helper.incident_review_needed?).to eq(true)
      end

      it 'returns true for review-requested, sev3 issues' do
        allow(helper).to receive(:resource).and_return(
          labels: ['review-requested', 'severity::3']
        )
        expect(helper.incident_review_needed?).to eq(true)
      end

      it 'returns true for review-requested, sev2 issues' do
        allow(helper).to receive(:resource).and_return(
          labels: ['review-requested', 'severity::2']
        )
        expect(helper.incident_review_needed?).to eq(true)
      end

      it 'returns true for review-requested, backstage issues' do
        allow(helper).to receive(:resource).and_return(
          labels: %w[review-requested backstage]
        )
        expect(helper.incident_review_needed?).to eq(true)
      end

      it 'returns true for sev1 issues' do
        allow(helper).to receive(:resource).and_return(
          labels: ['severity::1']
        )
        expect(helper.incident_review_needed?).to eq(true)
      end

      it 'returns true for sev2 issues' do
        allow(helper).to receive(:resource).and_return(
          labels: ['severity::2']
        )
        expect(helper.incident_review_needed?).to eq(true)
      end
    end

    context 'when there is a related issues with the incident-review label' do
      before do
        allow(helper).to receive(:query_issue_links).and_return(
          [
            { 'labels' => %w[some-label another-label] },
            { 'labels' => %w[some-label another-label incident-review] }
          ]
        )
      end

      it 'returns false for sev1 issues with an associated review' do
        allow(helper).to receive(:resource).and_return(
          labels: ['severity::1']
        )
        expect(helper.incident_review_needed?).to eq(false)
      end

      it 'returns false for sev2 issues with an associated review' do
        allow(helper).to receive(:resource).and_return(
          labels: ['severity::2']
        )
        expect(helper.incident_review_needed?).to eq(false)
      end

      it 'returns false for review-requested issues with an associated review' do
        allow(helper).to receive(:resource).and_return(
          labels: ['review-requested']
        )
        expect(helper.incident_review_needed?).to eq(false)
      end
    end
  end
end
