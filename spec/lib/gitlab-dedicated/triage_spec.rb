# frozen_string_literal: true

require 'spec_helper'

describe GitlabDedicatedTeam::Triage do
  let(:today) { Date.today }
  let(:tomorrow) { Date.tomorrow }
  let(:helper) { Class.new { extend GitlabDedicatedTeam::Triage } }

  before do
    allow(helper).to receive(:resource).and_return(
      state: 'opened',
      labels: ['Dedicated::Request for Help']
    )
    allow(helper).to receive(:assignees).and_return(
      ['some-gitlab-username']
    )
  end

  describe '.oncall_users_to_gitlab_users' do
    let(:oncalls) do
      [
        Pagerduty::User.new(name: 'Lou Segusi', email: 'lsegusi@gitlab.com'),
        Pagerduty::User.new(name: 'Viola Fuss', email: 'vfuss@gitlab.com')
      ]
    end

    before do
      allow(helper).to receive(:gitlab_users_map).and_return(
        {
          'Lou Segusi' => 'lsegusi',
          'Viola Fuss' => 'vfuss'
        }
      )
    end

    it 'returns correct oncalls' do
      expect(helper.oncall_users_to_gitlab_users("", oncalls)).to eq([%w[lsegusi vfuss], []])
    end

    it 'returns correct oncalls with ping notation' do
      expect(helper.oncall_users_to_gitlab_users("@", oncalls)).to eq([%w[@lsegusi @vfuss], []])
    end

    it 'returns missing oncalls' do
      oncalls_with_missing = oncalls + [Pagerduty::User.new(name: 'Lauren Order', email: 'lorder@gitlab.com')]
      expect(helper.oncall_users_to_gitlab_users("", oncalls_with_missing)).to eq([%w[lsegusi vfuss], ["Lauren Order"]])
    end

    context 'with users not having email addresses' do
      let(:oncalls) do
        [
          Pagerduty::User.new(name: 'Lou Segusi'),
          Pagerduty::User.new(name: 'Viola Fuss'),
          Pagerduty::User.new(name: 'Lauren Order')
        ]
      end

      before do
        allow(helper).to receive(:gitlab_users_map).and_return(
          {
            'Lou Segusi' => 'lsegusi',
            'Viola Fuss' => 'vfuss'
          }
        )
      end

      it 'correctly maps users and identifies missing ones' do
        expect(helper.oncall_users_to_gitlab_users("", oncalls)).to eq([%w[lsegusi vfuss], ["Lauren Order"]])
      end

      it 'correctly maps users with ping notation' do
        expect(helper.oncall_users_to_gitlab_users("@", oncalls)).to eq([%w[@lsegusi @vfuss], ["Lauren Order"]])
      end
    end
  end

  describe '.missing_rfh_assignees' do
    let(:assignees) do
      [
        {
          "id" => 1329,
          "name" => "Lou Segusi",
          "username" => "lsegusi",
          "state" => "active"
        }
      ]
    end

    before do
      allow(helper).to receive(:ea_oncalls_during).with(today, tomorrow).and_return(
        [
          Pagerduty::User.new(name: 'Lou Segusi', email: 'lsegusi@gitlab.com'),
          Pagerduty::User.new(name: 'Viola Fuss', email: 'vfuss@gitlab.com')
        ]
      )

      allow(helper).to receive(:gitlab_users_map).and_return(
        {
          'Lou Segusi' => 'lsegusi',
          'Viola Fuss' => 'vfuss'
        }
      )
    end

    it 'detects missing assignees' do
        expect(helper.missing_rfh_assignees(assignees)).to eq(%w[vfuss])
    end

    it 'returns empty array when all oncall users are assigned' do
      no_missing_assignees = assignees +
        [
          {
            "id" => 1348,
            "name" => "Viola Fuss",
            "username" => "vfuss",
            "state" => "active"
          }
        ]

      expect(helper.missing_rfh_assignees(no_missing_assignees)).to eq([])
    end

    it 'returns empty array when all oncall users are assigned even with extra assignees' do
      extra_assignees = assignees +
        [
          {
            "id" => 1348,
            "name" => "Viola Fuss",
            "username" => "vfuss",
            "state" => "active"
          },
          {
            "id" => 1457,
            "name" => "Lauren Order",
            "username" => "lorder",
            "state" => "active"
          }
        ]

      expect(helper.missing_rfh_assignees(extra_assignees)).to eq([])
    end
  end

  describe '.assignments_for_firedrills' do
    let(:firedrillers) do
      [
        Pagerduty::User.new(name: 'Lou Segusi'),
        Pagerduty::User.new(name: 'Viola Fuss')
      ]
    end

    before do
      allow(helper).to receive(:firedrillers).and_return(firedrillers)
      allow(helper).to receive(:gitlab_users_map).and_return(
        {
          'Lou Segusi' => 'lsegusi',
          'Viola Fuss' => 'vfuss'
        }
      )
    end

    context 'when there are firedrills scheduled' do
      before do
        allow(helper).to receive(:firedrill_this_week?).and_return(true)
      end

      it 'returns assignment comment with all firedrillers' do
        expected_comment = "/assign @lsegusi @vfuss"
        expect(helper.assignments_for_firedrills).to eq(expected_comment)
      end

      it 'handles missing gitlab users' do
        firedrillers_with_missing = firedrillers + [Pagerduty::User.new(name: 'Lauren Order')]
        allow(helper).to receive(:firedrillers).and_return(firedrillers_with_missing)

        expected_comment = <<~COMMENT.chomp
          /assign @lsegusi @vfuss
          Could not find GitLab users for Lauren Order
          Please update their name in PagerDuty to match their GitLab full name
        COMMENT

        expect(helper.assignments_for_firedrills).to eq(expected_comment)
      end
    end

    context 'when there are no firedrills scheduled' do
      before do
        allow(helper).to receive(:firedrill_this_week?).and_return(false)
      end

      it 'returns no firedrills message' do
        expect(helper.assignments_for_firedrills).to eq("No Firedrills this week")
      end
    end
  end

  describe '.firedrillers_table' do
    let(:firedrillers) do
      [
        Pagerduty::User.new(name: 'Lou Segusi'),
        Pagerduty::User.new(name: 'Viola Fuss')
      ]
    end

    before do
      allow(helper).to receive(:firedrillers).and_return(firedrillers)
      allow(helper).to receive(:gitlab_users_map).and_return(
        {
          'Lou Segusi' => 'lsegusi',
          'Viola Fuss' => 'vfuss'
        }
      )
    end

    it 'generates correct markdown table structure' do
      expected_table = "| Firedriller | Operator | Tenant | Status | RTO Time |\n" \
                      "|------------|----------|--------|--------|----------|\n" \
                      "| @lsegusi | | | | |\n" \
                      "| @vfuss | | | | |\n"

      expect(helper.firedrillers_table).to eq(expected_table)
    end

    it 'generates only header rows when firedrillers is empty' do
      allow(helper).to receive(:firedrillers).and_return([])

      expected_table = "| Firedriller | Operator | Tenant | Status | RTO Time |\n" \
                      "|------------|----------|--------|--------|----------|\n"

      expect(helper.firedrillers_table).to eq(expected_table)
    end
  end

  describe '.region_for_date' do
    let(:reference_date) { Date.new(2025, 1, 3) }  # First Friday of 2025

    it 'returns correct region for reference date' do
      expect(helper.region_for_date(reference_date)).to eq(:APAC)
    end

    it 'cycles through regions correctly' do
      expect(helper.region_for_date(reference_date + 7)).to eq(:EMEA)
      expect(helper.region_for_date(reference_date + 14)).to eq(:AMER)
      expect(helper.region_for_date(reference_date + 21)).to eq(:none)
      expect(helper.region_for_date(reference_date + 28)).to eq(:APAC)
    end
  end

  describe '.firedrill_this_week?' do
    it 'returns true when region is not :none' do
      allow(helper).to receive(:region_for_date).and_return(:APAC)
      expect(helper.firedrill_this_week?).to be true
    end

    it 'returns false when region is :none' do
      allow(helper).to receive(:region_for_date).and_return(:none)
      expect(helper.firedrill_this_week?).to be false
    end
  end

  describe '.past_due_date?' do
    before do
      allow(Date).to receive(:today).and_return(Date.parse('2025-01-01'))
    end

    it 'returns true when the due date is empty' do
      allow(helper).to receive(:resource).and_return(
        due_date: nil
      )
      expect(helper.past_due_date?).to eq(true)
    end

    it 'returns true when the due date is in the past' do
      allow(helper).to receive(:resource).and_return(
        due_date: '2024-12-01'
      )
      expect(helper.past_due_date?).to eq(true)
    end

    it 'returns false when the due date is in the future' do
      allow(helper).to receive(:resource).and_return(
        due_date: '2025-02-01'
      )

      expect(helper.past_due_date?).to eq(false)
    end

    it 'returns false when the due date is today' do
      allow(helper).to receive(:resource).and_return(
        due_date: '2025-01-01'
      )

      expect(helper.past_due_date?).to eq(false)
    end
  end
end
