These are policies to create issues in the [Scalability project], but
with summaries of issues from the [gitlab-org group].

[Scalability project]: https://gitlab.com/gitlab-com/gl-infra/scalability
[gitlab-org group]: https://gitlab.com/gitlab-org
