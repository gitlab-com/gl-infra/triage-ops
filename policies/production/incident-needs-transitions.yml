.needs_window: &needs_window
  date:
    attribute: created_at
    condition: newer_than
    interval_type: months
    interval: 1

resource_rules:
  issues:
    rules:
      - name: Adds RootCause::Needed
        conditions:
          <<: *needs_window
          ruby: |
            root_cause_needed?
          labels:
            - incident
            - severity::{1,2,3}
            - release-blocker
        actions:
          labels:
            - RootCause::Needed

      - name: Adds Service::Needed
        conditions:
          <<: *needs_window
          ruby: |
            service_needed?
          labels:
            - incident
            - severity::{1,2,3}
        actions:
          labels:
            - Service::Needed

      - name: Add CorrectiveActions::Needed
        conditions:
          <<: *needs_window
          ruby: |
            !corrective_actions?
          labels:
            - incident
            - severity::{1,2}
          forbidden_labels:
            - CorrectiveActions::Needed
            - CorrectiveActions::NotNeeded
        actions:
          labels:
            - CorrectiveActions::Needed
          comment: |
            Hi #{assignees.map { |u| "@#{u}" }.join(" ")},

            This issue now has the ~"CorrectiveActions::Needed" label, this label will be removed automatically when there is
            at least one related issue that is labeled with ~"corrective action" or ~"infradev".
            Having an issue related with these labels helps to ensure a similar incident doesn't happen again.

            If you are certain that this incident doesn't require any corrective actions, add the
            ~"CorrectiveActions::NotNeeded" label to this issue with a note explaining why.

            #{footer("incident-needs")}

      - name: Removes CorrectiveActions::Needed
        conditions:
          labels:
            - CorrectiveActions::Needed
          ruby: |
            corrective_actions?
        actions:
          remove_labels:
            - CorrectiveActions::Needed
      - name: Adds ExecSummary::Needed
        conditions:
          <<: *needs_window
          ruby: |
            !exec_summary?
          labels:
            - incident
            - severity::{1,2}
          forbidden_labels:
            - ExecSummary::Needed
            - ExecSummary::NotNeeded
          state: opened
        actions:
          labels:
            - ExecSummary::Needed
          comment: |
            Hi #{assignees.map { |u| "@#{u}" }.join(" ")},

            This issue now has the ~"ExecSummary::Needed" label, this label will be removed automatically when there
            the following section is added to the incident description and is filled out:

            ```
            ### :pencil: Summary for CMOC notice / Exec summary:

            1. Customer Impact: {+ Human-friendly 1-sentence statement on impacted +}
            1. Service Impact: {+ service:: labels of services impacted by this incident +}
            1. Impact Duration: {+ start time UTC +} - {+ end time UTC +} ({+ duration in minutes +})
            1. Root cause: {+ TBD +}

            ```

            Be sure to remove any braces, `{` or `}`, are removed from this section for it to be considered `filled out`.

            If you are certain that this incident doesn't require an exec summary, add the
            ~"ExecSummary::NotNeeded" label to this issue with a note explaining why.

            #{footer("incident-needs")}
      - name: Removes ExecSummary::Needed
        conditions:
          labels:
            - ExecSummary::Needed
          ruby: |
            exec_summary?
        actions:
          remove_labels:
            - ExecSummary::Needed
      - name: Add IncidentReview::Needed
        conditions:
          <<: *needs_window
          ruby: |
            incident_review_needed?
          labels:
            - incident
          forbidden_labels:
            - IncidentReview::Needed
            - IncidentReview::NotNeeded
          limit:
            most_recent: 10
        actions:
          labels:
            - IncidentReview::Needed
          comment: |
            Hi #{assignees.map { |u| "@#{u}" }.join(" ")},

            Thanks for taking part in this incident! It looks like this incident needs
            an async Incident Review issue, please use the _Incident Review_ link in
            the incident's description to create one.

            We're posting this message because this issue meets the following criteria:

            * It is ~severity::1 / ~severity::2, or has a ~review-requested label
            * There is no related issue with an ~incident-review label

            If you are certain that this incident doesn't require an incident review, add the
            ~"IncidentReview::NotNeeded" label to this issue with a note explaining why.

            Thanks for your help! :black_heart:

            ----
            #{footer("activity-reminders-change-incidents")}

      - name: Removes IncidentReview::Needed
        conditions:
          labels:
            - IncidentReview::Needed
          ruby: |
            !incident_review_needed?
        actions:
          remove_labels:
            - IncidentReview::Needed

      - name: Adds Mention for overdue Needed labeled issues
        conditions:
          date:
            attribute: updated_at
            condition: older_than
            interval_type: days
            interval: 7
          ruby: |
            needed_label?
          labels:
            - incident
            - severity::{1,2}
        actions:
          comment: |
            Hi #{needed_mentions.map { |u| "@#{u}" }.join(" ")}

            This issue has #{needed_labels.map { |l| "~\"#{l}\"" }.join(" / ")} set.
            Please either fix this by adding one of the corresponding labels, or add a `::NotNeeded` scoped label with an explanation
            if you are sure that it is not needed. Adding the `::NotNeeded` scoped label will prevent these notifications, otherwise
            this notice will repeat in 7 days.

            Once the criteria is met, #{needed_labels.map { |l| "~\"#{l}\"" }.join(" / ")} label will be removed automatically.
            See the [handbook page on `::Needed` and `::NotNeeded` labels](https://handbook.gitlab.com/handbook/engineering/infrastructure/incident-management/#needed-and-notneeded-scoped-labels) for more information about how the labels are set and removed.

            #{footer("incident-needs")}
      - name: Add review-requested label to S1 issues
        conditions:
          labels:
            - incident
            - severity::1
          forbidden_labels:
            - review-requested
        actions:
          labels:
            - review-requested

      - name: Close incident issues if they meet the conditions for closing
        # For high severity (sev1/sev2) we require no needs labels on incidents before closing
        # For other incidents we will close as soon as they are resolved
        conditions:
          labels:
            - Incident::Resolved
          ruby: |
            high_severity? ? !needed_label? : true
          state: opened
        actions:
          status: close
          comment: |
            This incident was automatically closed because it has the ~"Incident::Resolved" label.

            **Note**: All incidents are closed automatically when they are resolved, even when there is a pending
            review. Please see the [Incident Workflow](https://about.gitlab.com/handbook/engineering/infrastructure/incident-management/#incident-workflow)
            section on the Incident Management handbook page for more information.

      - name: Relabel closed incidents that are ~"Incident::{Active,Mitigated}" to ~"Incident::Resolved"
        conditions:
          labels:
            - Incident::{Active,Mitigated}
          state: closed
        actions:
          labels:
            - Incident::Resolved
          status: close
