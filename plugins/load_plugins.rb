# frozen_string_literal: true

Dir["#{__dir__}/*.rb"].sort.each do |plugin|
  next if plugin.end_with?('load_plugins.rb')

  require_relative File.basename(plugin, '.rb')
end
