# frozen_string_literal: true

require_relative File.expand_path('../lib/platform/triage.rb', __dir__)
require_relative File.expand_path('../lib/gitlab-dedicated/triage.rb', __dir__)

Gitlab::Triage::Resource::Context.include Platform::Triage
Gitlab::Triage::Resource::Context.include GitlabDedicatedTeam::Triage
