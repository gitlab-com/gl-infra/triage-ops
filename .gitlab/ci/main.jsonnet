local project = import 'project.libsonnet';

local projects = [
  {
    name: 'durability',
    source_opts: '--source-id gitlab-com/gl-infra/data-access/durability/team',
    plugins: ['durability.rb'],
    sched_var: '$INFRASTRUCTURE_PROJECT',
    platform_policies: [
      'add-infradev-to-ca.yml',
      'infradev.yml',
      'infrafin.yml',
      'in-progress.yml',
      'service-unknown.yml',
      'workflow-done-to-closed.yml',
      'close-workflow-done-cancelled.yml',
      'workflow-stalled.yml',
      'close-discussion.yml',
      'slo-infradev.yml',
      'workflow-blocked.yml',
      'staff-plus-label.yml',
      'workflow-triage.yml',
    ],
  },

  {
    name: 'production-engineering',
    source_opts: '--source-id gitlab-com/gl-infra/production-engineering',
    plugins: ['production-engineering.rb'],
    sched_var: '$INFRASTRUCTURE_PROJECT',
    platform_policies: [
      'add-infradev-to-ca.yml',
      'infradev.yml',
      'infrafin.yml',
      'in-progress.yml',
      'service-unknown.yml',
      'workflow-done-to-closed.yml',
      'close-workflow-done-cancelled.yml',
      'workflow-stalled.yml',
      'close-discussion.yml',
      'slo-infradev.yml',
      'workflow-blocked.yml',
      'staff-plus-label.yml',
      'workflow-triage.yml',
    ],
  },
  {
    name: 'production',
    source_opts: '--source-id gitlab-com/gl-infra/production',
    plugins: ['production.rb'],
    sched_var: '$PRODUCTION_PROJECT',
    platform_policies: [
      'slo-on-incident-review.yml',
    ],
  },
  {
    name: 'production_often',
    source_opts: '--source-id gitlab-com/gl-infra/production',
    plugins: ['production.rb'],
    sched_var: '$PRODUCTION_PROJECT_OFTEN',
  },
  {
    name: 'delivery',
    source_opts: '--source-id gitlab-com/gl-infra/delivery',
    plugins: ['delivery.rb'],
    sched_var: '$DELIVERY_PROJECT',
    platform_policies: [
      'add-infradev-to-ca.yml',
      'infradev.yml',
      'infrafin.yml',
      'in-progress.yml',
      'close-workflow-done-cancelled.yml',
      'workflow-stalled.yml',
      'close-discussion.yml',
      'slo-infradev.yml',
      'workflow-blocked.yml',
      'staff-plus-label.yml',
      'workflow-triage.yml',
    ],
  },
  {
    name: 'gitlab-dedicated',
    source_opts: '--source-id gitlab-com/gl-infra/gitlab-dedicated/team',
    plugins: ['gitlab-dedicated.rb'],
    sched_var: '$GITLAB_DEDICATED_PROJECT',
    platform_policies: [
      'infrafin.yml',
      'in-progress.yml',
      'workflow-done-to-closed.yml',
      'close-workflow-done-cancelled.yml',
      'workflow-stalled.yml',
      'close-discussion.yml',
      'workflow-blocked.yml',
      'workflow-triage.yml',
    ],
  },
  {
    name: 'gitlab-dedicated-incident-management',
    source_opts: '--source-id gitlab-com/gl-infra/gitlab-dedicated/incident-management',
    plugins: [],
    sched_var: '$GITLAB_DEDICATED_INCIDENT_MANAGEMENT_PROJECT',
  },
  {
    name: 'gitlab-dedicated-rfh',
    source_opts: '--source-id gitlab-com/request-for-help',
    plugins: ['gitlab-dedicated.rb'],
    sched_var: '$RFH_PROJECT',
  },
  {
    name: 'on-call-handovers',
    source_opts: '--source-id gitlab-com/gl-infra/on-call-handovers',
    plugins: [],
    sched_var: '$ON_CALL_HANDOVERS_PROJECT',
  },
  {
    name: 'on-call-onboarding',
    source_opts: '--source-id gitlab-com/gl-infra/production-engineering',
    plugins: [],
    sched_var: '$ON_CALL_ONBOARDING_PROJECT',
  },
  {
    name: 'scalability',
    source_opts: '--source-id gitlab-com/gl-infra/scalability',
    plugins: ['scalability.rb'],
    sched_var: '$SCALABILITY_PROJECT',
    platform_policies: [
      'add-infradev-to-ca.yml',
      'infradev.yml',
      'infrafin.yml',
      'service-unknown.yml',
      'close-workflow-done-cancelled.yml',
      'workflow-stalled.yml',
      'close-discussion.yml',
      'workflow-blocked.yml',
      'slo-infradev.yml',
      'staff-plus-label.yml',
      'workflow-triage.yml',
    ],
  },
  {
    name: 'scalability-gitlab-org',
    source_opts: '--source-id gitlab-org --source groups',
    plugins: ['scalability.rb'],
    sched_var: '$SCALABILITY_PROJECT',
  },
  {
    name: 'reliability-reports',
    source_opts: '--source-id gitlab-com/gl-infra/reliability-reports',
    plugins: [],
    sched_var: '$INFRASTRUCTURE_PROJECT',
  },
];

{
  'projects.gitlab-ci.yml': std.manifestYamlDoc(
    std.foldl(function(x, y) x + y, [
      project.new(
        proj,
        'dry-run',
        [{ 'if': '$CI_COMMIT_BRANCH != $CI_DEFAULT_BRANCH' }],
      ) +
      project.new(
        proj,
        'manual',
        [{ 'if': '$CI_COMMIT_BRANCH != $CI_DEFAULT_BRANCH', when: 'manual', allow_failure: true }],
      ) +
      project.new(
        proj,
        'run',
        [{ 'if': std.format('$CI_PIPELINE_SOURCE == "schedule" && %s == "1"', proj.sched_var) }],
      )
      for proj in projects
    ], {})
  ),
}
