{
  new(proj, stage, rules=[])::
    local stage_opts = if stage == 'dry-run' then
      '--dry-run'
    else
      '';
    local plugins_opts = std.join(' ', std.map(function(plugin) std.format('-r ./plugins/%s', plugin), proj.plugins));
    {
      [std.format('%s:%s', [stage, proj.name])]: {
        stage: stage,
        script: [
          std.format('find policies/%s -type f -name *.yml | xargs -n1 bundle exec gitlab-triage %s --debug --token $GITLAB_BOT_AUTOMATED_TRIAGE_TOKEN %s %s -f',
            [
              proj.name,
              stage_opts,
              proj.source_opts,
              plugins_opts,
            ]
          ),
        ] + std.map(function(policy)
          std.format('bundle exec gitlab-triage %s --debug --token $GITLAB_BOT_AUTOMATED_TRIAGE_TOKEN %s %s -f policies/platform/%s',
            [
              stage_opts,
              proj.source_opts,
              plugins_opts,
              policy,
            ]
          ), std.get(proj, 'platform_policies', [])),
        rules: rules,
      },
    },
}
