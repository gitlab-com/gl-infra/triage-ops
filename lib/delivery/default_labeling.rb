# frozen_string_literal: true

class DeliveryTeam
  module Triage
    extend self
    SKIP_LABELS = %w[
      onboarding
      Discussion
      Announcements
      access-token
    ]

    PROJECT_LABELS = [
      'kubernetes',
      'deployer',
      'patcher',
      'AutoDeploy',
      'release toil',
      'corrective action',
      'security-release',
      'release tooling',
      'documentation',
      'security',
      'tooling',
      'independent deployments',
      'internal releases',
      'metrics',
      'Category:Cell',
      'Dedicated deployments',
      'Release Environments'
    ]

    PRIORITY_LABELS = [
      'Delivery::P1',
      'Delivery::P2',
      'Delivery::P3',
      'Delivery::P4'
    ]

    AUTOMATION_LABEL = 'auto updated'

    TEAM_LABEL = 'group::delivery'

    TEAM_HANDLE = 'gitlab-org/delivery'

    WORKFLOW_LABEL_PREFIX = 'workflow-infra::'

    DEFAULT_WORKFLOW_LABEL = 'workflow-infra::Triage'

    def has_team_task_label?
      labels.any? { |label| SKIP_LABELS.include?(label) }
    end

    def has_team_label?
      labels.any?(TEAM_LABEL)
    end

    def has_project_label?
      labels.any? { |label| PROJECT_LABELS.include?(label) }
    end

    def has_priority_label?
      labels.any? { |label| PRIORITY_LABELS.include?(label) }
    end

    def has_workflow_label?
      !labels.grep(/^#{WORKFLOW_LABEL_PREFIX}/).empty?
    end

    def has_automation_label?
      labels.any?(AUTOMATION_LABEL)
    end

    def has_project_or_task_label?
      has_team_task_label? || has_project_label?
    end

    def required_labels
      ensured_labels = []
      ensured_labels << TEAM_LABEL unless has_team_label?
      ensured_labels << DEFAULT_WORKFLOW_LABEL unless has_workflow_label?
      ensured_labels << PRIORITY_LABELS.last unless has_priority_label?
      ensured_labels << AUTOMATION_LABEL if !has_automation_label? && !has_project_or_task_label?

      ensured_labels
    end

    def quick_actions_labeling
      "/label #{labels_with_markdown}"
    end

    def unknown_project_mention
      return '' if has_project_or_task_label?
      return '' if has_automation_label?

      <<~SERVICE_TEXT
        Hi @#{author_username},

        Every issue without one of the following labels:
        #{unfurled_markdown_labels(SKIP_LABELS)}
        requires at least one project label.

        Please apply one of:

        #{unfurled_markdown_labels(PROJECT_LABELS)}

        to this issue.

        Thanks! :purple_heart: \n
        --
        :collaboration: Feel free to [improve me](https://gitlab.com/gitlab-com/gl-infra/triage-ops/-/blob/master/lib/delivery/#{__FILE__})
      SERVICE_TEXT
    end

    def add_comment
      [unknown_project_mention, quick_actions_labeling].join("\n")
    end

    private

    def labels
      resource[:labels]
    end

    def author_username
      @author_username ||= resource.dig(:author, :username) || TEAM_HANDLE
    end

    def unfurled_markdown_labels(label_set)
      label_set.map { |label| markdown_label(label) }.join(', ')
    end

    def labels_with_markdown
      required_labels.map(&Triage.method(:markdown_label)).join(' ')
    end

    def markdown_label(label)
      %(~"#{label}")
    end
  end
end
