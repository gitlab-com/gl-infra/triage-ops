# frozen_string_literal: true

class ScalabilityTeam
  module Triage
    extend self
    TRIAGE_OWNER = 'stejacks-gitlab'

    SKIP_LABELS = [
      'Status Report',
      'onboarding',
      'team-tasks',
      'Discussion',
      'Capacity Planning'
    ]

    GROUP_LABEL = 'group::scalability'

    GROUP_LABEL_PREFIX = 'group::'
    WORKFLOW_LABEL_PREFIX = 'workflow-infra::'
    SERVICE_LABEL_PREFIX = 'Service::'

    TEAM_HANDLE = 'gitlab-org/scalability'

    def has_team_task_label?
      labels.any? { |label| SKIP_LABELS.include?(label) }
    end

    def has_workflow_label?
      !labels.grep(/^#{WORKFLOW_LABEL_PREFIX}/).empty?
    end

    def required_labels
      ensured_labels = []
      ensured_labels << GROUP_LABEL unless has_group_label?

      # Team tasks don't need anything else
      return ensured_labels if has_team_task_label?

      ensured_labels << "#{WORKFLOW_LABEL_PREFIX}Triage" unless has_workflow_label?

      # Category labels don't need a service
      return ensured_labels if has_category_label?

      ensured_labels << "#{SERVICE_LABEL_PREFIX}Unknown" unless has_service_label?

      ensured_labels
    end

    def quick_actions_labeling
      "/label #{labels_with_markdown}"
    end

    def unknown_service_mention
      return '' if has_team_task_label? || has_service_label? || has_category_label?

      <<~SERVICE_TEXT
        Hi @#{author_username},

        Every issue without one of the following labels:
        #{unfurled_markdown_labels(SKIP_LABELS)} or a `Category:` label
        requires a scoped `Service::` label.

        We've temporarily assigned a #{markdown_label('Service::Unknown')} label.
        If you know which service this issue refers to, we would kindly ask you to
        apply the correct label based on
        [labels definitions](https://about.gitlab.com/handbook/engineering/infrastructure/team/scalability/#labels).

        Thanks! :green_heart: \n
      SERVICE_TEXT
    end

    def add_comment
      [unknown_service_mention, quick_actions_labeling].join("\n")
    end

    def update_triage_rotation_message
      return '' unless last_week_of_month?

      <<~UPDATE_MESSAGE
        Please also update the `TRIAGE_OWNER` constant in
        https://gitlab.com/gitlab-com/gl-infra/triage-ops/blob/master/lib/scalability/triage.rb
        to point to next month's triage owner. Thanks for triaging this
        month!
      UPDATE_MESSAGE
    end

    private

    def labels
      resource[:labels]
    end

    def author_username
      @author_username ||= resource.dig(:author, :username) || TEAM_HANDLE
    end

    def unfurled_markdown_labels(label_set)
      label_set.map { |label| markdown_label(label) }.join(', ')
    end

    def labels_with_markdown
      required_labels.map(&Triage.method(:markdown_label)).join(' ')
    end

    def markdown_label(label)
      %(~"#{label}")
    end

    def last_week_of_month?
      today = Date.today

      (Date.new(today.year, today.month, -1) - today) < 7
    end

    def footer(policy)
      <<~FOOTER
        ----

        You are welcome to help [improve this comment](https://gitlab.com/gitlab-com/gl-infra/triage-ops/blob/master/policies/scalability/#{policy}.yml).
      FOOTER
    end
  end
end
