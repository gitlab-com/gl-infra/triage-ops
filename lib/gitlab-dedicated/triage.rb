# frozen_string_literal: true

require 'pagerduty'
require 'gitlab'

class GitlabDedicatedTeam
  module Triage
    extend self

    SKIP_LABELS = [
      'team-tasks'
    ].freeze

    EA_TEAM_LABEL = 'team::Environment Automation'
    SWITCHBOARD_TEAM_LABEL = 'team::Switchboard'

    TEAM_LABEL_PREFIX = 'team::'
    WORKFLOW_LABEL_PREFIX = 'workflow-infra::'
    COMPONENT_LABEL_PREFIX = 'component::'
    SWITCHBOARD_COMPONENT_LABEL = 'component::Switchboard'

    TEAM_HANDLE = 'gitlab-com/gl-infra/gitlab-dedicated'
    EA_GROUP_NAME = 'gitlab-dedicated/environment-automation'
    PUBSEC_GROUP_NAME = 'gitlab-dedicated/uspubsec'

    PLATFORM_ONCALL_SCHEDULE = 'PE57MNA'

    REGIONS = %i(APAC EMEA AMER none)  # none represents a gap week for issue scheduling

    def team_label_assigned?
      !labels.grep(/^#{TEAM_LABEL_PREFIX}/).empty?
    end

    def workflow_label_assigned?
      !labels.grep(/^#{WORKFLOW_LABEL_PREFIX}/).empty?
    end

    def skip_label_assigned?
      labels.any? { |label| SKIP_LABELS.include?(label) }
    end

    def component_label_assigned?
      !labels.grep(/^#{COMPONENT_LABEL_PREFIX}/).empty?
    end

    def switchboard_component_label_assigned?
      !labels.grep(/^#{SWITCHBOARD_COMPONENT_LABEL}/).empty?
    end

    def required_labels
      ensured_labels = []

      unless team_label_assigned?
        ensured_labels << if switchboard_component_label_assigned?
                            SWITCHBOARD_TEAM_LABEL
                          else
                            EA_TEAM_LABEL
                          end
      end

      ensured_labels << "#{WORKFLOW_LABEL_PREFIX}Triage" if !skip_label_assigned? && !workflow_label_assigned?

      ensured_labels
    end

    def quick_actions_labeling
      "/label #{labels_with_markdown}"
    end

    def add_comment
      [quick_actions_labeling].join("\n")
    end

    def iso_date(num_to_skip)
      (Date.today + num_to_skip).iso8601
    end

    def toil_issue_title
      "#{iso_date(3)} - Upgrade tooling in tenant environments (PRODUCTION)"
    end

    def oncall_handover_issue_title
      "#{iso_date(8)} - On-Call Handover"
    end

    def firedrill_issue_title
      "#{iso_date(1)} - EA Firedrill: Geo Failover"
    end

    def pubsec_operations_issue?
      resource.dig(:epic, :title) == 'US Public Sector Services General Operations'
    end

    def oncall_users_to_gitlab_users(pattern="", oncalls)
      missing = []
      assignees = []
      oncalls.map(&:name).compact.each do |name|
        if gitlab_users_map.key?(name)
          assignees << pattern + gitlab_users_map[name]
        else
          missing << name
        end
      end
      [assignees, missing]
    end

    def assignments_for_current_oncalls
      current_oncalls = ea_oncalls_during(Date.today, Date.tomorrow)
      assignees, missing = oncall_users_to_gitlab_users("@", current_oncalls)
      oncall_assignment_comment(assignees, missing)
    end

    def checklist_for_current_oncalls(shift_length_in_days=7)
      shift_oncalls = ea_oncalls_during(Date.today, iso_date(shift_length_in_days))
      outgoing_eocs, missing = oncall_users_to_gitlab_users("- [ ] @", shift_oncalls)
      outgoing_eocs.join("\n")
    end

    def assignments_for_firedrills
      return "No Firedrills this week" unless firedrill_this_week?

      assignees, missing = oncall_users_to_gitlab_users("@", firedrillers)
      oncall_assignment_comment(assignees, missing)
    end

    def missing_rfh_assignees(assignees)
      current_oncalls = ea_oncalls_during(Date.today, Date.tomorrow)
      eocs, missing = oncall_users_to_gitlab_users("", current_oncalls)
      eocs - assignees.pluck("username").compact
    end

    def firedrillers_table
      # Create header row
      table = "| Firedriller | Operator | Tenant | Status | RTO Time |\n"
      table += "|------------|----------|--------|--------|----------|\n"

      # Add a row for each firedriller
      drillers, missing = oncall_users_to_gitlab_users("@", firedrillers)
      drillers.each do |firedriller|
        table += "| #{firedriller} | | | | |\n"
      end

      table
    end

    def region_for_date(date)
      # Using first Friday of 2025 as reference date (2025-01-03)
      reference_date = Date.new(2025, 1, 3)

      # Calculate the number of weeks since ref date
      weeks_since_reference = ((date - reference_date).to_i / 7)

      # Use modulo to cycle through regions
      REGIONS[weeks_since_reference % REGIONS.length]
    end

    def firedrill_this_week?
      region_for_date(Date.today) != :none
    end

    def past_due_date?
      due_date = resource[:due_date]

      # Return true if there is no due date
      return true unless due_date != nil

      Date.parse(due_date) < Date.today
    end

    private

    def labels
      resource[:labels]
    end

    def labels_with_markdown
      required_labels.map(&Triage.method(:markdown_label)).join(' ')
    end

    def markdown_label(label)
      %(~"#{label}")
    end

    def pagerduty
      @pagerduty ||= Pagerduty.new(token: ENV.fetch('PAGERDUTY_READONLY_TOKEN', nil), subdomain: 'gitlab')
    end

    def gitlab_client
      @gitlab_client ||= Gitlab.client(
        endpoint: 'https://gitlab.com/api/v4',
        private_token: ENV.fetch('GITLAB_BOT_AUTOMATED_TRIAGE_TOKEN', nil)
      )
    end

    def ea_group_members
      @ea_group_members ||= gitlab_client.all_group_members(EA_GROUP_NAME)
    end

    def pubsec_group_members
      @pubsec_group_members ||= gitlab_client.all_group_members(PUBSEC_GROUP_NAME)
    end

    def gitlab_users_map
      @gitlab_users_map ||= (ea_group_members + pubsec_group_members).to_h do |member|
        [member['name'], member['username']]
      end
    end

    def ea_oncalls_during(start_date, end_date)
      pagerduty.get_schedule_users(id: PLATFORM_ONCALL_SCHEDULE, since: start_date, until: end_date)
    end

    def ea_oncalls_in_region_since(region, start_date)
      return [] if region == :none

      response = pagerduty.get_schedule(id: PLATFORM_ONCALL_SCHEDULE, since: start_date, until: start_date + 60)
      layer = response['schedule_layers'].find { |layer| layer['name'].upcase == region.to_s.upcase }
      users = layer['rendered_schedule_entries'].map { |entry| entry['user'] }.uniq

      if users
        users.map { |user_entry| Pagerduty::User.new(name: user_entry['summary']) }.uniq
      else
        []
      end
    end

    def oncall_assignment_comment(assignees, missing)
      lines = ["/assign #{assignees.join(' ')}"]
      if missing.any?
        lines << "Could not find GitLab users for #{missing.join(' ')}"
        lines << 'Please update their name in PagerDuty to match their GitLab full name'
      end
      lines.join("\n")
    end

    def firedrillers
      return @firedrillers if @firedrillers

      start_date = Date.today
      firedrill_region = region_for_date(start_date)
      firedrillers = ea_oncalls_in_region_since(firedrill_region, start_date)
    end
  end
end
