# frozen_string_literal: true

class Durability
  module Triage
    extend self

    def labels
      resource[:labels]
    end

    def footer(policy)
      <<~FOOTER
        ----

        You are welcome to help [improve this comment](https://gitlab.com/gitlab-com/gl-infra/triage-ops/blob/master/policies/durability/#{policy}.yml).
      FOOTER
    end
  end
end
