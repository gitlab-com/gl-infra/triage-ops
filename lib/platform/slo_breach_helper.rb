# frozen_string_literal: true

class Platform
  module SloBreachHelper
    extend self

    SLO_WARNING_PERCENTAGE = 50.0

    # https://about.gitlab.com/handbook/engineering/quality/issue-triage/#severity-slos
    DEFAULT_SEVERITY_SLO_TARGETS = {
      'severity::1' => 7,
      'severity::2' => 30,
      'severity::3' => 60,
      'severity::4' => 90
    }.freeze

    INCIDENT_REVIEW_SLO_TARGET = 14

    MENTION_FOR_TEAM_LABEL = {
      'team::Scalability-Observability' => '@gitlab-org/scalability/observability',
      'team::Scalability-Practices' => '@gitlab-org/scalability/practices',
      'team::Ops' => '@gitlab-org/production-engineering/ops',
      'team::Foundations' => '@gitlab-org/production-engineering/foundations',
    }.freeze

    def slo_breach_looming?
      return false if slo_breached?

      slo_looming_date < today
    end

    def mentions_for_slo
      (assignees.map { |a| "@#{a}" } + [MENTION_FOR_TEAM_LABEL[team_label]]).compact.join(' ')
    end

    def slo_breached?
      slo_breach_date < today
    end

    def severity_label
      resource[:labels].find { |l| l.start_with?('severity::') }
    end

    def team_label
      resource[:labels].find { |l| l.start_with?('team::') }
    end

    def slo_breach_warning_threshold(severity_label)
      DEFAULT_SEVERITY_SLO_TARGETS[severity_label] * (SLO_WARNING_PERCENTAGE / 100)
    end

    def slo_days_til_breach
      (slo_breach_date - today).to_i
    end

    def slo_breached_incident_review?
      slo_breach_date_incident_review < today
    end

    def slo_start_date_incident_review
      resource[:created_at].to_datetime
    end

    def slo_breach_date_incident_review
      slo_start_date_incident_review + INCIDENT_REVIEW_SLO_TARGET
    end

    def slo_looming_date_incident_review
      slo_start_date_incident_review + (INCIDENT_REVIEW_SLO_TARGET * (SLO_WARNING_PERCENTAGE / 100))
    end

    def slo_breach_looming_incident_review?
      return false if slo_breached_incident_review?

      slo_looming_date_incident_review < today
    end

    def slo_update_due_date_incident_review?
      resource[:due_date] != slo_breach_date_due_incident_review
    end

    def slo_breach_date_due_incident_review
      slo_breach_date_incident_review.strftime('%Y-%m-%d')
    end

    def slo_start_date
      # In order to use labels_with_details helper we need to allow confidential data,
      # otherwise for confidential issues it will be nil
      @redact_confidentials = false
      severity_label_with_details = labels_with_details.find { |l| l.name.start_with?('severity') }
      severity_label_with_details&.added_at&.to_date || resource[:created_at].to_datetime
    end

    def slo_breach_date
      slo_start_date + DEFAULT_SEVERITY_SLO_TARGETS[severity_label]
    end

    def slo_looming_date
      slo_start_date + slo_breach_warning_threshold(severity_label)
    end

    def slo_update_due_date?
      resource[:due_date] != slo_breach_date_due
    end

    def slo_breach_date_due
      slo_breach_date.strftime('%Y-%m-%d')
    end

    def today
      @today ||= Date.today
    end
  end
end
