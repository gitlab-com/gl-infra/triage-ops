# frozen_string_literal: true

class Platform
  module Triage
    extend self

    def is_blocked?
      query_issue_links.any? do |issue|
        issue['state'] == 'opened' && issue['link_type'] == 'is_blocked_by'
      end
    end

    def query_issue_links
      @issue_links ||= @network.query_api_cached(
        resource_url(sub_resource_type: 'links')
      )
    end

    def has_workflow_infra_label?
      labels.any? { |label| label.downcase.start_with?('workflow-infra::') }
    end

    def has_severity_label?
      labels.any? { |label| label.downcase.start_with?('severity::') }
    end

    def has_priority_label?
      labels.any? { |label| label.downcase.start_with?('priority::') }
    end

    def has_service_label?
      labels.any? { |label| label.downcase.start_with?('service::') }
    end

    def has_team_label?
      labels.any? { |label| label.downcase.start_with?('team::') }
    end

    def has_group_label?
      labels.any? { |label| label.downcase.start_with?('group::') }
    end

    def has_category_label?
      labels.any? { |label| label.downcase.start_with?('category:') }
    end

    def author
      resource.dig(:author, :username)
    end

    def assignees
      resource[:assignees].map { |assignee| assignee[:username] }
    end

    def assignees_or_author
      assignees.any? ? assignees : [author]
    end

    def footer_platform(policy)
      <<~FOOTER
        ----

        You are welcome to help [improve this comment](https://gitlab.com/gitlab-com/gl-infra/triage-ops/blob/master/policies/platform/#{policy}.yml).
      FOOTER
    end
  end
end
