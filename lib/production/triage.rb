# frozen_string_literal: true

class Production
  module Triage
    extend self

    ROOT_CAUSE_LABEL_PREFIX = 'RootCause::'
    SERVICE_LABEL_PREFIX = 'Service::'
    CORRECTIVE_ACTION_LABELS = ['corrective action', 'infradev'].freeze
    PROD_ENG_LEADERSHIP = %w[kkyrala jarv].freeze
    DELIVERY_LEADERSHIP = %w[dawsmith mbursi].freeze
    REVIEW_COMPLETED_LABEL = 'Incident::Review-Completed'
    INCIDENT_REVIEW_LABEL = 'incident-review'
    REVIEW_REQUESTED_LABEL = 'review-requested'
    RELEASE_BLOCKER_LABEL = 'release-blocker'
    BACKSTAGE_LABEL = 'backstage'
    HIGH_SEVERITY_LABELS = ['severity::1', 'severity::2'].freeze
    EXEC_SUMMARY_FIELDS = [
      'Customer Impact',
      'Service Impact',
      'Impact Duration',
      'Root cause'
    ].freeze
    def needed_labels
      labels.grep(/::Needed$/)
    end

    def needed_label?
      needed_labels.any?
    end

    def high_severity?
      !(labels & HIGH_SEVERITY_LABELS).empty?
    end

    def root_cause_needed?
      labels.grep(/^#{ROOT_CAUSE_LABEL_PREFIX}/).empty?
    end

    def service_needed?
      labels.grep(/^#{SERVICE_LABEL_PREFIX}/).empty?
    end

    def needed_mentions
      assignees.concat(PROD_ENG_LEADERSHIP).tap do |mentions|
        mentions.concat(DELIVERY_LEADERSHIP) if labels.include?(RELEASE_BLOCKER_LABEL)
      end
    end

    def query_issue_links
      @query_issue_links ||= @network.query_api_cached(
        resource_url(sub_resource_type: 'links')
      )
    end

    def corrective_actions?
      query_issue_links.any? do |issue|
        !(issue['labels'] & CORRECTIVE_ACTION_LABELS).empty?
      end
    end

    def imoc_is?(imoc_username)
      resource[:assignees].any? { |assignee| assignee[:username] == imoc_username }
    end

    def no_reviewready?
      description.match?(%r{#+ Corrective Actions[^#]+?\n[-*1-9 ][^\n]+https://}m) ||
        description.match?(/(Service\(s\) affected:\n)|(Team attribution:\n)|(Time to detection:\n)|(Minutes downtime or degradation:\n)/) ||
        description.match?(/internal customers\)\*\*. *1\. \.\.\./m)
    end

    def incident_review_completed?
      query_issue_links.any? do |issue|
        issue['labels'].include?(REVIEW_COMPLETED_LABEL)
      end
    end

    def incident_review_needed?
      return false if !review_requested? && labels.include?(BACKSTAGE_LABEL)

      return false if !review_requested? && (labels & HIGH_SEVERITY_LABELS).empty?

      query_issue_links.none? { |issue| issue['labels'].include?(INCIDENT_REVIEW_LABEL) }
    end

    def weekly_report?
      Date.today.wday == 1 && Time.now.hour == 9
    end

    def exec_summary?
      # For every field we ensure that the field name exists, followed by a ':' followed by any
      # character that is not a '{'
      EXEC_SUMMARY_FIELDS.map { |f| description.match?(/#{f}\s*:(?!\s*{)/i) }.all?
    end

    private

    def review_requested?
      labels.include?(REVIEW_REQUESTED_LABEL)
    end

    def description
      resource[:description]
    end

    def labels
      resource[:labels]
    end

    def assignees
      resource[:assignees].map { |assignee| assignee[:username] }
    end

    def footer(policy)
      <<~FOOTER
        ----

        You are welcome to help [improve this comment](https://gitlab.com/gitlab-com/gl-infra/triage-ops/blob/master/policies/production/#{policy}.yml).
      FOOTER
    end
  end
end
