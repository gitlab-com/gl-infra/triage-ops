# frozen_string_literal: true

class ProductionEngineeringTeam
  module Triage
    extend self

    def labels
      resource[:labels]
    end

    def footer(policy)
      <<~FOOTER
        ----

        You are welcome to help [improve this comment](https://gitlab.com/gitlab-com/gl-infra/triage-ops/blob/master/policies/production-engineering/#{policy}.yml).
      FOOTER
    end
  end
end
