.PHONY: jsonnet-tool
jsonnet-tool:
ifeq (, $(shell command -v jsonnet-tool))
$(error "No jsonnet-tool in $(PATH), install following https://gitlab.com/gitlab-com/gl-infra/asdf-gl-infra")
endif
.PHONY: generate-ci-config
generate-ci-config: jsonnet-tool
	@jsonnet-tool render \
		--jpath '.gitlab/ci/lib' \
		--multi '.gitlab/ci/' \
		--header '# auto-generated with `make generate-ci-config`' \
		.gitlab/ci/main.jsonnet

.PHONY: validate-ci-config
validate-ci-config: generate-ci-config
	(git diff --exit-code) || (echo "Please run 'make generate-ci-config' and commit the changes" && exit 1)
